// Створіть програму секундомір.
           
// * Секундомір матиме 3 кнопки "Старт, Стоп, Скидання"
// * При натисканні на кнопку стоп фон секундоміра має бути червоним, старт - зелений, скидання - сірий
// * Виведення лічильників у форматі ЧЧ:ММ:СС
// * Реалізуйте Завдання використовуючи синтаксис ES6 та стрілочні функції

window.onload = () =>{
    let sec = 0, m = 0, hrs = 0, intervalCounter, flag = false;
  

    const get = id => document.getElementById(id);

    const counter = () => {
        get("sp_3").innerHTML = sec;
       
        get("sp_1").innerHTML = hrs;
        get("sp_2").innerHTML = m;

            sec++;
        if(sec >= 60){
            sec = 0;
            m++;
        }if( m >= 60){
            m = 0;
            hrs++;

        }if(hrs >= 24){
            hrs = 0;
        } 
    }
    const button_1 = document.getElementById("btn_1");

    button_1.onclick = () => {
        const div = document.getElementById("div_par");

        div.classList.add("green");
        if (!flag) {
            intervalCounter = setInterval(counter, 10);
            flag = true;
        }
    }
    const button_2 = document.getElementById("btn_2");

    button_2.onclick = () =>{
        const div = document.getElementById("div_par");
        div.classList.remove("green");
        div.classList.add("red");
        clearInterval(intervalCounter);
        flag = false;
    }

    const button_3 = document.getElementById("btn_3");

    button_3.onclick = () =>{
        const div = document.getElementById("div_par");
        div.classList.remove("red");
        div.classList.add("silver");
        
        const [...reset] = document.getElementsByTagName("span");

        reset.forEach(el =>{
            el.textContent = "00";  
        }
        ) 
    }

}