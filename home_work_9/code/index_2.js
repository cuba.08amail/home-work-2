// Реалізуйте програму перевірки телефону
//             * Використовуючи JS Створіть поле для введення телефону та кнопку збереження
//             * Користувач повинен ввести номер телефону у форматі 0(Початок з 0)ХХ-ХХХ-ХХ-ХХ 

//             * Після того як користувач натискає кнопку зберегти перевірте правильність введення номера, якщо номер
//             правильний
//             зробіть зелене тло і використовуючи document.location переведіть через 3с користувача на сторінку
//             https://w.forfun.com/fetch/94/94c56e15f13f1de4740a76742b0b594f.jpeg
//             якщо буде помилка, відобразіть її в діві до input.

window.onload = () => {

    alert("Введіть номер телефону у форматі 0ХХ-ХХХ-ХХ-ХХ");

    const div = document.getElementById("divs");

    const input = document.createElement("input");
    input.className = "inp";
    input.value = "0ХХ-ХХХ-ХХ-ХХ"
    input.id = "inp_1";
    div.after(input);

    const button = document.createElement("button");
    button.textContent = "Зберігти"
    button.classList.add("btn");
    button.id = "btn_1";
    div.after(button);

    button.onclick = function () {

        const user = document.getElementById("inp_1");
       
        const pattern = /\b[0]\d{2}-\d{3}-\d{2}-\d{2}/;
        tel = user.value;
        // tel = "068-162-86-17";
        if (pattern.test(tel)) {

            user.classList.add("green");
            function fn() {
                document.location = "https://w.forfun.com/fetch/94/94c56e15f13f1de4740a76742b0b594f.jpeg"
            }
            setTimeout(fn, 3000);
        }
        else {
            const div = document.createElement("div");
            div.textContent = "Невірно введено формат телефону";
            input.before(div);
        }
    }
}