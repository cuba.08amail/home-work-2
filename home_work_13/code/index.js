
/*Задача знайти api монобанк курс валют, нова пошта віділення та погода. 
 1 Напишіть JavaScript-функцію, яка виконує GET запит до відкритого API за допомогою Fetch.
 2 Функція повинна приймати URL API як вхідний параметр.
 3 Використовуйте Fetch, щоб виконати GET запит за вказаною URL.
 4 Обробляйте отриману відповідь у форматі JSON та виводьте результати в консоль.
 5 Обробляйте можливі помилки під час виконання запиту та виводьте відповідне повідомлення про помилку.
 моно банк:
  https://api.monobank.ua/bank/currency
  
  нова пошта:
  https://api.novaposhta.ua/v2.0/json/

  погода:
  http://api.weatherapi.com/v1/current.json?key=7a44623160324719a9595130230507&q=Ukraine&aqi=no
 */

const monoBank = (url) =>
    new Promise((resolve, reject) =>
        fetch(url)
        .then(response => response.json())
        .then(json => resolve(json))
        .catch(error => reject(error))
    )

    monoBank('https://api.monobank.ua/bank/currency')
.then(data => console.log(data))
.catch(error => console.log(error.message));


// const getMonoBank = async (url) =>{
//     const res = await fetch(url);
//     const json = await res.json()
//     return json
// }

// const url = 'https://api.monobank.ua/bank/currency'

// try{
//     const data = await getMonoBank(url)
//     console.log(data)
// } catch (error){
// console.log(error.message)
// }









