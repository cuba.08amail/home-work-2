
// нова пошта:
// JSON - https://api.novaposhta.ua/v2.0/json/

const novPost = (url) =>
    new Promise((resolve, reject) =>
        fetch(url)
        .then(response => response.json())
        .then(json => resolve(json))
        .catch(error => reject(error))
    )

    novPost('https://api.novaposhta.ua/v2.0/json/')
.then(data => console.log(data))
.catch(error => console.log(error.message));


