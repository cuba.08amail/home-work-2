/*  погода: 
  http://api.weatherapi.com/v1/current.json?key=7a44623160324719a9595130230507&q=Ukraine&aqi=no
  */ 

const weatherApi = (url) =>
    new Promise((resolve, reject) =>
        fetch(url)
        .then(response => response.json())
        .then(json => resolve(json))
        .catch(error => reject(error))
    )

    weatherApi('http://api.weatherapi.com/v1/current.json?key=7a44623160324719a9595130230507&q=Ukraine&aqi=no')
.then(data => console.log(data))
.catch(error => console.log(error.message));