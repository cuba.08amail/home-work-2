/*4) Даний рядок типу 'var_text_hello'. Зробіть із нього текст 'VarTextHello'. */

function text(str) {
return str[0].toUpperCase() + str.substr(1);}
var str = 'var_text_hello';
var res = [];

var newArr = str.split('_');
res.push(newArr[0]);

 for (let i = 1; i < newArr.length; i++) {
   var newStr = text(newArr[i]);
   res.push(newStr);
 }

 var rez = res.join('');

 console.log(rez);

/*5) Функція ggg приймає 2 параметри: анонімну функцію, яка повертає 3 та анонімну функцію, яка
повертає 4. Поверніть результатом функції ggg суму 3 та 4.*/

var ggg = function(x, y){
    return x() + y();
};
var f1 = function(){
    return 3;
};
var f2 = function(){
    return 4;
};
console.log(ggg(f1, f2));


/*6) Створіть об'єкт криптокошилок. У гаманці має зберігатись ім'я власника, кілька валют Bitcoin, Ethereum Stellar і в кожній валюті додатково є ім'я валюти, логотип, кілька монет та курс на сьогоднішній день. Також в об'єкті гаманець є метод при виклику якого він приймає ім'я валюти та виводить на сторінку  інформацію."Доброго дня, ... ! На вашому балансі (Назва валюти та логотип) залишилося N монет, якщо ви сьогодні продасте їх те, отримаєте ...грн. */

            const wallet = {
    name: "Alina",
    bitcoin: {
        name:"bitcoin", 
        logo:"<img src='https://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Bitcoin.svg/1200px-Bitcoin.svg.png'width='50' height='50' />", 
        coin:"25",
        rate:"30245"
    },
    etherium: {
        name:"Etherium", 
        logo:"<img src='https://upload.wikimedia.org/wikipedia/commons/0/01/Ethereum_logo_translucent.svg' width='50' height='50' />", 
        coin:"50",
        rate:"1801"
    },
    stellar: {
        name:"Stellar", 
        logo:"<img src='https://s2.coinmarketcap.com/static/img/coins/200x200/512.png' width='50' height='50' />", 
        coin:"75",
        rate:"0.1402"
    },
    show: function(nameCoin){
        document.getElementById("wallet1").innerHTML=`Добрый день ${wallet.name}, на вашем балансе ${wallet[nameCoin].name} ${wallet[nameCoin].logo} осталось ${wallet[nameCoin].coin} монет , если вы сегодня продадите их, то получите ${wallet[nameCoin].coin*wallet[nameCoin].rate*42} грн.`
    }
}
wallet.show(prompt("Введіть назви монет:", "bitcoin, etherium, stellar"));

/*
7) Реалізуйте клас Worker (Працівник), який матиме такі властивості: name (ім'я), surname (прізвище),
rate (ставка за день роботи), days (кількість відпрацьованих днів).
Також клас повинен мати метод getSalary(), який виводитиме зарплату працівника.
Зарплата - це добуток (множення) ставки rate на кількість відпрацьованих днів days. */

function Worker(){
    this.name = "Nik";
    this.surname = "Goji";
    this.rate = 1000;
    this.days = 22;

    this.getSalary = function(){
        return this.rate*this.days;
    }
}
let work = new Worker();
console.log(work.name);
console.log(work.surname);
console.log(work.rate);
console.log(work.days);
console.log(work.getSalary());


