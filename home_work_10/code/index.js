// * У папці calculator дана верстка макета калькулятора.
// Потрібно зробити цей калькулятор робочим.
// * При натисканні на клавіші з цифрами - набір введених цифр має бути показаний на табло калькулятора.
// * При натисканні на знаки операторів (`*`, `/`, `+`, `-`) на табло нічого не відбувається - програма чекає введення другого числа для виконання операції.
// * Якщо користувач ввів одне число, вибрав оператор і ввів друге число, то при натисканні як кнопки `=`, так і будь-якого з операторів, в табло повинен з'явитися результат виконання попереднього виразу.
// * При натисканні клавіш `M+` або `M-` у лівій частині табло необхідно показати маленьку букву `m` - це означає, що в пам'яті зберігається число. Натискання на MRC покаже число з пам'яті на екрані. Повторне натискання `MRC` має очищати пам'ять.


window.addEventListener("DOMContentLoaded", function () {
    let a = '';
    let b = '';
    let sing = '';
    let res = false;

    const arrNumber = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.'];
    const arrSing = ['+', '-', '*', '/'];

    const display = document.getElementById("dis");
    const memory = document.getElementById("memory");

    display.value = 0;

    document.querySelectorAll("input").forEach((e) => {
        e.addEventListener("click", () => {

            const key = e.value;

            if (arrNumber.includes(key)) {
                if (b === '' && sing === '') {
                    a += key;
                    display.value = a;
                }

                if (a !== '' && sing !== '') {
                    b += key;
                    display.value = b;
                }
            }

            if (arrSing.includes(key)) {
                sing = key;
                display.value = '';
                return
            }

            if (key === '=') {
                if (b === '') b = a
                switch (sing) {
                    case '+':
                        res = parseFloat(a) + parseFloat(b);
                        break
                    case '-':
                        res = parseFloat(a) - parseFloat(b);
                        break
                    case '*':
                        res = parseFloat(a) * parseFloat(b);
                        break
                    case '/':
                        res = parseFloat(a) / parseFloat(b);
                        break
                }

                display.value = a = res;
                b = '';
                sing = '';
            }
        });
    });

    const reset = document.getElementById('reset');

    reset.addEventListener('click', () => {
        a = '';
        b = '';
        sing = '';
        res = false;
        display.value = 0;
        memory.innerHTML = '';
    });


    //Определяем переменную в которую будем сохранять значение
    let memM1 = '';
    //Навешиваем событие клика на элемент m1
    document.getElementById('m1').addEventListener('click', () => {
        //Текущее значение на экране записываем в переменную для памяти
        memM1 = a = display.value;
        //Отображаем букву в блоке для понимание что мы ее записали
        memory.innerHTML = 'm-';
        //Стираем отображение но память осталась в memM1
        display.value = '';
    });


    //Определяем переменную в которую будем сохранять значение
    let memM2 = '';
    //Навешиваем событие клика на элемент m2
    document.getElementById('m2').addEventListener('click', () => {
        //Текущее значение на экране записываем в переменную для памяти
        memM2 = a = display.value;
        //Отображаем букву в блоке для понимание что мы ее записали
        display.value = memM2;
        //Стираем отображение но память осталась в memM2
        memory.innerHTML = 'm+';
    });


    // на mrc выдает ошибку инициализации.
    // Нужно чтобы было как в задаче:
    // При натисканні клавіш `M+` або `M-` у лівій частині табло
    // необхідно показати маленьку букву `m` - це означає, що в пам'яті зберігається число.
    // Натискання на MRC покаже число з пам'яті на екрані. Повторне натискання `MRC` має очищати пам'ять.


    //Устанавливаем счетчик в 0
    let countMrs = 0;
    document.getElementById('mrc').addEventListener('click', () => {
        //Увеличиваем счетчик при каждом клике на +1
        countMrs++;
        if (countMrs >= 2) {
            //Если он достиг 2  - то обнуляем память согласно условия
            memM1 = memM2 = '';
            //Так же обнуляем дисплей
            display.value = '';

        }
        //Если есть что показать из памяти в переменной 1 то показываем
        if (memM1 !== '') {
            display.value = a = memM1;
        }
        //Если есть что показать из памяти в переменной 2 то показываем
        if (memM2 !== '') {
            display.value = a = memM2;
        }
        //После того как вытащили переменную из памяти затираем букву М+ или М- на дисплее
        memory.innerHTML = '';
    });
})

