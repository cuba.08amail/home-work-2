/*Створіть сайт з коментарями. 
    Коментарі тут : https://jsonplaceholder.typicode.com/
    Сайт має виглядати так : https://kondrashov.online/images/screens/120.png
    На сторінку виводити по 100 коментарів, у низу сторінки зробити поле пагінації (перемикання сторінок) при перемиканні
    сторінок показувати нові коментарі. 
    з коментарів виводити : 
    "id": 1,
    "name"
    "email"
    "body":*/

function req(url, callback) {
    const a = new XMLHttpRequest();

    a.open('GET', url);
    a.send();
    a.addEventListener('readystatechange', () => {
        if (a.readyState === 4 && a.status >= 200 && a.status < 300) {
            // console.log( a.response);
            callback(JSON.parse(a.response));

        } else if (a.readyState === 4) {
            alert('error');
        }
    });
}

req('https://jsonplaceholder.typicode.com/comments', show);

let commentsPage = 100;
let limit = 100;
let start = 0;

function show(data = []) {
    if (!Array.isArray(data)) {
        return;
    }

    const div = document.querySelector('.container');

    data.forEach((obj, i) => {
       
        if (i >= start && i < limit) {
            card = `<div id="number-${i}" class="number">${i + 1}
                        <div class="card">${obj.id}
                            <h3 class="name">${obj.name}</h3>
                            <p class="body">${obj.body}</p>
                            <a href="#" class="email">${obj.email}</a>
                        </div>
                    </div>`;
            div.insertAdjacentHTML('beforeend', card);
        }
    });
}
document.querySelectorAll("button").forEach((e) => {
    e.addEventListener('click', () => {

        let pageNumber = e.innerText || e.textContent;
        console.log(pageNumber)

        limit = commentsPage * pageNumber;
        start = commentsPage * pageNumber - commentsPage;

        //Очищаем блок предыдущих значений
        let nodes = document.getElementsByClassName("number");
        let arrayNodes = Array.from(nodes);
        arrayNodes.forEach((element) => {
            element.remove();
        });

        req('https://jsonplaceholder.typicode.com/comments', show);
    });
});
